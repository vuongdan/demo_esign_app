import 'package:flutter_bloc/flutter_bloc.dart';

class AppBlocs {
  // static final homeBloc = HomeBloc();
  // static final loginBloc = LoginBloc();
  // static final registerBloc = RegisterBloc();
  // static final forgotPasswordBloc = ForgotPasswordBloc();

  // BLOC Providers
  static final List<BlocProvider> blocProviders = [
    // BlocProvider<LoginBloc>(create: (_) => loginBloc),
    // BlocProvider<HomeBloc>(create: (_) => homeBloc),
    // BlocProvider<RegisterBloc>(create: (_) => registerBloc),
    // BlocProvider<ForgotPasswordBloc>(create: (_) => forgotPasswordBloc),
  ];

  static void dispose() {}

  static void logOut() {}

  static void runApp() {
    cleanBloc();
  }

  static void initialHomeBlocWithoutAuth() {}

  static void initialHomeBlocWithAuth() {}

  static void cleanBloc() {}

  /// Singleton Factory
  static final AppBlocs _instance = AppBlocs._internal();

  factory AppBlocs() {
    return _instance;
  }

  AppBlocs._internal();
}
