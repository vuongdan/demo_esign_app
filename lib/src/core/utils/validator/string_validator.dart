String? isValidDateString(String? date) {
  final RegExp regExp =
      RegExp(r'^([0-2][0-9]|(3)[0-1])(\/)(((0)[0-9])|((1)[0-2]))(\/)\d{4}$');
  try {
    final int? dateString = int.tryParse((date ?? '').substring(0, 2));
    final int? monthString = int.tryParse((date ?? '').substring(3, 5));
    if (dateString != null &&
        monthString != null &&
        regExp.hasMatch(date ?? '') &&
        dateString <= 31 &&
        dateString > 0 &&
        monthString <= 12 &&
        monthString > 0) {
      return null;
    } else {
      return "Bạn vui lòng nhập ngày sinh hợp lệ";
    }
  } catch (e) {
    return "Bạn vui lòng nhập ngày sinh hợp lệ";
  }
}

String? isValidNameString(String? name) {
  if (name == null || name.isEmpty) {
    return "Bạn vui lòng nhập họ tên";
  }
  return null;
}

String? isValidPhoneString(String? phone) {
  final RegExp regExp = RegExp(r'^\d{10}$');
  try {
    if (regExp.hasMatch(phone ?? '') &&
        ((phone ?? '').substring(0, 1) == "0")) {
      return null;
    } else {
      return "Bạn vui lòng nhập số điện thoại hợp lệ";
    }
  } catch (e) {
    return "Bạn vui lòng nhập số điện thoại hợp lệ";
  }
}

String? isValidEmailString(String? email) {
  final RegExp regExp = RegExp(
      r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$');
  try {
    if (regExp.hasMatch(email ?? '')) {
      return null;
    } else {
      return "Bạn vui lòng nhập email hợp lệ";
    }
  } catch (e) {
    return "Bạn vui lòng nhập email hợp lệ";
  }
}

String? isValidPasswordString(String? password) {
  if (password == null || password.isEmpty || password.length < 8) {
    return "Mật khẩu phải có ít nhất 8 ký tự";
  }
  return null;
}

String? isValidConfirmPasswordString(
    String? password, String? confirmPassword) {
  if (confirmPassword == null ||
      confirmPassword.isEmpty ||
      confirmPassword.length < 8) {
    return "Mật khẩu phải có ít nhất 8 ký tự";
  }
  if (password != confirmPassword) {
    return "Mật khẩu không trùng khớp";
  }
  return null;
}

String? isValidIDString(String? idCard) {
  final RegExp regExp9 = RegExp(r'^\d{9}$');
  final RegExp regExp12 = RegExp(r'^\d{12}$');
  try {
    if (regExp12.hasMatch(idCard ?? '') || regExp9.hasMatch(idCard ?? '')) {
      return null;
    } else {
      return "Bạn vui lòng nhập CMND/CCCD hợp lệ";
    }
  } catch (e) {
    return "Bạn vui lòng nhập CMND/CCCD hợp lệ";
  }
}

String? isValidUsernameString(String? username) {
  if (username != null &&
      username.isNotEmpty &&
      (username).indexOf(" ") == -1) {
    return null;
  }
  return "Tên đăng nhập không hợp lệ";
}
