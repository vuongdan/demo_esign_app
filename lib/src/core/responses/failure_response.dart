// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'dart:convert';

class Failure {}

class ServerFailure extends Failure {}

class FailureResponse extends Failure {
  FailureResponse({
    required this.data,
    required this.status,
    required this.total,
    required this.message,
  });

  final Data? data;
  final int status;
  final int total;
  final String message;

  FailureResponse copyWith({
    Data? data,
    int? status,
    int? total,
    String? message,
  }) {
    return FailureResponse(
      data: data ?? this.data,
      status: status ?? this.status,
      total: total ?? this.total,
      message: message ?? this.message,
    );
  }

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'data': data?.toMap(),
      'status': status,
      'total': total,
      'message': message,
    };
  }

  factory FailureResponse.fromMap(Map<String, dynamic> map) {
    return FailureResponse(
      data: map['data'] != null
          ? Data.fromMap(map['data'] as Map<String, dynamic>)
          : null,
      status: map['status'] as int,
      total: map['total'] as int,
      message: map['message'] as String,
    );
  }

  String toJson() => json.encode(toMap());

  factory FailureResponse.fromJson(String source) =>
      FailureResponse.fromMap(json.decode(source) as Map<String, dynamic>);

  @override
  String toString() {
    return 'FailureResponse(data: $data, status: $status, total: $total, message: $message)';
  }

  @override
  bool operator ==(covariant FailureResponse other) {
    if (identical(this, other)) return true;

    return other.data == data &&
        other.status == status &&
        other.total == total &&
        other.message == message;
  }

  @override
  int get hashCode {
    return data.hashCode ^ status.hashCode ^ total.hashCode ^ message.hashCode;
  }
}

class Data {
  Data();

  Map<String, dynamic> toMap() {
    return <String, dynamic>{};
  }

  factory Data.fromMap(Map<String, dynamic> map) {
    return Data();
  }

  String toJson() => json.encode(toMap());

  factory Data.fromJson(String source) =>
      Data.fromMap(json.decode(source) as Map<String, dynamic>);

  @override
  String toString() => 'Data()';
}
