import 'dart:convert';

class CommonResponse {
  CommonResponse({
    required this.data,
    required this.status,
    required this.total,
    required this.message,
  });

  CommonResponseData? data;
  int status;
  int total;
  String message;

  factory CommonResponse.fromJson(String str) =>
      CommonResponse.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory CommonResponse.fromMap(Map<String, dynamic> json) => CommonResponse(
        data: json["data"] == null
            ? null
            : CommonResponseData.fromMap(json["data"]),
        status: json["status"] == null ? null : json["status"],
        total: json["total"] == null ? null : json["total"],
        message: json["message"] == null ? null : json["message"],
      );

  Map<String, dynamic> toMap() => {
        "data": data?.toMap(),
        "status": status,
        "total": total,
        "message": message,
      };
}

class CommonResponseData {
  CommonResponseData({
    required this.timeOtp,
  });

  DateTime timeOtp;

  factory CommonResponseData.fromJson(String str) =>
      CommonResponseData.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory CommonResponseData.fromMap(Map<String, dynamic> json) =>
      CommonResponseData(
        timeOtp: DateTime.parse(json["timeOTP"]),
      );

  Map<String, dynamic> toMap() => {
        "timeOTP": timeOtp.toIso8601String(),
      };
}
