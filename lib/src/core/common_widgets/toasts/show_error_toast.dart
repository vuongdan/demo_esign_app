import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:demo_esign_app/src/core/common_widgets/toasts/dialog_animation_wrapper.dart';
import 'package:demo_esign_app/src/core/constants/int_constants.dart';
import 'package:demo_esign_app/src/core/design/app_asset_links.dart';
import 'package:demo_esign_app/src/core/design/app_colors.dart';
import 'package:demo_esign_app/src/core/design/app_text_styles.dart';
import 'package:demo_esign_app/src/core/utils/sizer/extension.dart';

showToast(ToastType type, {String title = 'Có lỗi xảy ra'}) async {
  await dialogAnimationWrapper(
      type: type,
      timeForDismiss: 1500,
      slideFrom: SlideMode.top,
      dismissible: false,
      child: ErrorToast(
        type: type,
        title: title,
      ));
}

class ErrorToast extends StatelessWidget {
  const ErrorToast({
    Key? key,
    this.title,
    this.body,
    required this.type,
  }) : super(key: key);
  final String? title;
  final String? body;
  final ToastType type;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.transparent,
      body: SafeArea(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Container(
              width: 100.w,
              padding:
                  EdgeInsets.symmetric(horizontal: DEFAULT_HORIZONTAL_PADDING),
              child: Container(
                width: 100.w - 20,
                decoration: BoxDecoration(
                  color: AppColors.R1,
                  borderRadius: BorderRadius.circular(8),
                ),
                padding: EdgeInsets.symmetric(
                    horizontal: DEFAULT_HORIZONTAL_PADDING, vertical: 16),
                child: type == ToastType.error
                    ? Row(
                        children: [
                          SvgPicture.asset(
                            AppAssetLinks.ic_error_textfield,
                            width: 24,
                            height: 24,
                            fit: BoxFit.fill,
                          ),
                          SizedBox(width: 16),
                          Container(
                            width: 100.w - 104,
                            child: Text(
                              title ?? '',
                              style: AppTextStyles.body.fw400().fcR6(),
                            ),
                          ),
                        ],
                      )
                    : Row(
                        children: [
                          SvgPicture.asset(
                            AppAssetLinks.ic_tick_verify,
                            width: 24,
                            height: 24,
                            fit: BoxFit.fill,
                          ),
                          SizedBox(width: 16),
                          Container(
                            width: 100.w - 104,
                            child: Text(
                              title ?? '',
                              style: AppTextStyles.body.fw400().fcGr5(),
                              overflow: TextOverflow.ellipsis,
                            ),
                          ),
                        ],
                      ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
