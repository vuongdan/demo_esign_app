import 'package:flutter/material.dart';
import 'package:demo_esign_app/src/navigations/app_navigation.dart';

enum SlideMode {
  left,
  top,
  bot,
  right,
  fade,
}

enum ToastType {
  success,
  error,
}

Future dialogAnimationWrapper({
  required ToastType type,
  SlideMode slideFrom = SlideMode.left,
  child,
  duration = 400,
  paddingTop = 0.0,
  paddingBottom = 0.0,
  backgroundColor = Colors.white,
  paddingHorizontal = 15.0,
  borderRadius = 25.0,
  dismissible = true,
  barrierColor,
  timeForDismiss,
}) {
  var beginOffset = Offset(-1, 0);
  switch (slideFrom) {
    case SlideMode.left:
      beginOffset = Offset(-1, 0);
      break;
    case SlideMode.right:
      beginOffset = Offset(1, 0);
      break;
    case SlideMode.top:
      beginOffset = Offset(0, -1);
      break;
    default:
      beginOffset = Offset(0, 1);
      break;
  }

  if (timeForDismiss != null) {
    Future.delayed(Duration(milliseconds: timeForDismiss), () {
      AppNavigator.pop();
    });
  }

  return showGeneralDialog(
    barrierLabel: "Barrier",
    transitionDuration: Duration(milliseconds: duration),
    context: AppNavigator.context!,
    pageBuilder: (_, __, ___) {
      return child;
    },
    transitionBuilder: (_, anim, __, child) {
      return SlideTransition(
        position: Tween(begin: beginOffset, end: Offset(0, 0)).animate(anim),
        child: child,
      );
    },
  );
}
