// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

import 'package:demo_esign_app/src/core/constants/int_constants.dart';
import 'package:demo_esign_app/src/core/design/app_asset_links.dart';
import 'package:demo_esign_app/src/core/design/app_colors.dart';
import 'package:demo_esign_app/src/core/design/app_text_styles.dart';
import 'package:demo_esign_app/src/core/utils/sizer/extension.dart';

class ButtonPrimaryGradient extends StatelessWidget {
  const ButtonPrimaryGradient({
    Key? key,
    required this.onTap,
    required this.text,
  }) : super(key: key);
  final void Function() onTap;
  final String text;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Container(
          height: 44,
          width: 100.w - DEFAULT_HORIZONTAL_PADDING * 2,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(8),
              gradient: AppColors.ColorGadientOrange),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                text,
                style: AppTextStyles.body.fw600().fcW5(),
              ),
              SizedBox(
                width: 14,
              ),
              SvgPicture.asset(
                AppAssetLinks.ic_arrow_open,
                color: AppColors.W5,
              )
            ],
          )),
    );
  }
}
