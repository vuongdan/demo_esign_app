import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:demo_esign_app/src/core/design/app_asset_links.dart';
import 'package:demo_esign_app/src/core/design/app_colors.dart';

class CheckboxPrimary extends StatelessWidget {
  const CheckboxPrimary({
    Key? key,
    required this.isChecked,
    required this.onTongleCheckboxStatus,
  }) : super(key: key);
  final bool isChecked;
  final void Function() onTongleCheckboxStatus;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTongleCheckboxStatus,
      child: Container(
        height: 16,
        width: 16,
        decoration: BoxDecoration(
            border: Border.all(
              width: 1,
              color: AppColors.N8,
            ),
            borderRadius: BorderRadius.circular(2)),
        child: Center(
            child: isChecked
                ? SvgPicture.asset(AppAssetLinks.ic_tick_box)
                : SizedBox()),
      ),
    );
  }
}
