// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:flutter/material.dart';
import 'package:demo_esign_app/src/core/design/app_colors.dart';

class CheckBoxSelected extends StatelessWidget {
  const CheckBoxSelected({
    Key? key,
    required this.isSelected,
  }) : super(key: key);
  final bool isSelected;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 16,
      width: 16,
      decoration: BoxDecoration(
        color: AppColors.N2,
        borderRadius: BorderRadius.circular(8),
        border: Border.all(
          color: isSelected ? AppColors.Pr6 : AppColors.N5,
          width: 0.5,
        ),
      ),
      child: Center(
        child: Container(
          width: 8,
          height: 8,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(4),
            color: isSelected ? AppColors.Pr6 : AppColors.N2,
          ),
        ),
      ),
    );
  }
}
