// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:flutter/material.dart';

import 'package:demo_esign_app/src/core/constants/int_constants.dart';
import 'package:demo_esign_app/src/core/design/app_colors.dart';
import 'package:demo_esign_app/src/core/design/app_text_styles.dart';
import 'package:demo_esign_app/src/core/utils/sizer/extension.dart';

class ButtonDoubleChoice extends StatelessWidget {
  const ButtonDoubleChoice({
    Key? key,
    this.firstColor = AppColors.Pr6,
    this.secondColor = AppColors.W5,
    required this.firstText,
    required this.secondText,
    required this.firstOnTap,
    required this.secondOnTap,
  }) : super(key: key);
  final Color firstColor;
  final Color secondColor;
  final String firstText;
  final String secondText;
  final void Function() firstOnTap;
  final void Function() secondOnTap;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 44,
      width: 100.w - DEFAULT_HORIZONTAL_PADDING * 2,
      child: Row(children: [
        GestureDetector(
          onTap: firstOnTap,
          child: Container(
            width: 43.6.w,
            height: 44,
            decoration: BoxDecoration(
              color: firstColor,
              borderRadius: BorderRadius.circular(4),
            ),
            child: Center(
              child: Text("$firstText",
                  style:
                      AppTextStyles.body.fw600().copyWith(color: secondColor)),
            ),
          ),
        ),
        Spacer(),
        GestureDetector(
          onTap: secondOnTap,
          child: Container(
            width: 43.6.w,
            height: 44,
            decoration: BoxDecoration(
              color: secondColor,
              borderRadius: BorderRadius.circular(4),
              border: Border.all(
                color: firstColor,
                width: 1,
              ),
            ),
            child: Center(
              child: Text("$secondText",
                  style:
                      AppTextStyles.body.fw600().copyWith(color: firstColor)),
            ),
          ),
        ),
      ]),
    );
  }
}
