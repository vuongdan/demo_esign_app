// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:flutter/material.dart';

import 'package:demo_esign_app/src/core/constants/int_constants.dart';
import 'package:demo_esign_app/src/core/design/app_colors.dart';
import 'package:demo_esign_app/src/core/design/app_text_styles.dart';
import 'package:demo_esign_app/src/core/utils/sizer/extension.dart';

class ButtonPrimary extends StatelessWidget {
  const ButtonPrimary({
    Key? key,
    required this.onTap,
    required this.text,
    this.isTransparent = false,
  }) : super(key: key);
  final void Function() onTap;
  final String text;
  final bool isTransparent;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Container(
        height: 44,
        width: 100.w - DEFAULT_HORIZONTAL_PADDING * 2,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(8),
          color: isTransparent ? AppColors.W5 : AppColors.Pr6,
          border: Border.all(
            width: 1,
            color: isTransparent ? AppColors.Pr7 : AppColors.Pr6,
          ),
        ),
        child: Center(
          child: Text(
            text,
            style: AppTextStyles.body.fw600().copyWith(
                  color: isTransparent ? AppColors.Pr7 : AppColors.W5,
                ),
          ),
        ),
      ),
    );
  }
}
