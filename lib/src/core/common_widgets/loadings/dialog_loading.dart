import 'package:flutter/material.dart';
import 'package:demo_esign_app/src/core/design/app_colors.dart';
import 'package:demo_esign_app/src/navigations/app_navigation.dart';

showDialogLoading({bool activeCircle = true}) {
  showDialog(
    useSafeArea: false,
    context: AppNavigator.context!,
    builder: (context) {
      return activeCircle ? DialogLoading() : Container();
    },
  );
}

class DialogLoading extends StatelessWidget {
  const DialogLoading({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: double.infinity,
      width: double.infinity,
      color: AppColors.Dark,
      child: Center(
        child: CircularProgressIndicator(
          color: Color(0XFFD6E4FF),
        ),
      ),
    );
  }
}
