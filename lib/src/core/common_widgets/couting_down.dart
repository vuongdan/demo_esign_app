import 'package:flutter/material.dart';
import 'package:demo_esign_app/src/core/constants/int_constants.dart';
import 'package:demo_esign_app/src/core/design/app_colors.dart';
import 'package:demo_esign_app/src/core/design/app_text_styles.dart';
import 'package:demo_esign_app/src/core/utils/sizer/extension.dart';

class OTPCountingWidget extends StatelessWidget {
  OTPCountingWidget({
    Key? key,
    required this.duration,
    required this.maxDuration,
  }) : super(key: key);
  final int duration;
  final int maxDuration;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 58,
      width: 100.w - DEFAULT_HORIZONTAL_PADDING * 2,
      padding: EdgeInsets.all(DEFAULT_HORIZONTAL_PADDING),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(7),
        color: AppColors.Pr4,
      ),
      child: Column(
        children: [
          Align(
            alignment: Alignment.centerLeft,
            child: Text(
              "Mã OTP có hiệu lực trong: ${duration} s",
              style: AppTextStyles.small.fw400().fcPr11(),
            ),
          ),
          SizedBox(
            height: 8,
          ),
          Container(
            height: 2,
            width: 100.w - DEFAULT_HORIZONTAL_PADDING * 2,
            child: Row(
              children: [
                Expanded(
                  flex: duration,
                  child: Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(1),
                      color: AppColors.Pr6,
                    ),
                  ),
                ),
                Expanded(
                  flex: maxDuration - duration,
                  child: Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(1),
                      color: AppColors.W5,
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
