// ignore_for_file: public_member_api_docs, sort_constructors_first
// ignore_for_file: must_be_immutable

import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

import 'package:demo_esign_app/src/core/design/app_asset_links.dart';
import 'package:demo_esign_app/src/core/design/app_colors.dart';
import 'package:demo_esign_app/src/core/design/app_text_styles.dart';

class TextFieldCommon extends StatefulWidget {
  TextFieldCommon({
    Key? key,
    this.controller,
    required this.labelText,
    required this.hintText,
    required this.errorText,
    this.obscureText = false,
    this.prefixIcon = null,
    this.suffixIcon = null,
    required this.onChanged,
    this.enable = true,
    this.minLines = 1,
    this.maxLines = 1,
    this.isRequired = false,
    this.readOnly = false,
    this.onTap = null,
  }) : super(key: key);
  final TextEditingController? controller;
  final bool enable;
  String? labelText;
  String? hintText;
  String? errorText;
  bool obscureText;
  Widget? prefixIcon;
  Widget? suffixIcon;
  void Function(String?)? onChanged;
  int minLines;
  int maxLines;
  bool isRequired;
  bool readOnly;
  void Function()? onTap;

  @override
  State<TextFieldCommon> createState() => _TextFieldCommonState();
}

class _TextFieldCommonState extends State<TextFieldCommon> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        TextFormField(
          onTap: widget.onTap,
          readOnly: widget.readOnly,
          style: AppTextStyles.body.fw400().fcN10(),
          onChanged: widget.onChanged,
          controller: widget.controller,
          enabled: widget.enable,
          minLines: widget.minLines,
          maxLines: widget.maxLines,
          autovalidateMode: AutovalidateMode.onUserInteraction,
          obscureText: widget.obscureText,
          decoration: InputDecoration(
            label: Container(
              height: 16,
              child: Row(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Text(
                    widget.labelText ?? '',
                    style: AppTextStyles.small.fw400().copyWith(
                        color:
                            (widget.errorText == null || widget.errorText == '')
                                ? AppColors.N7
                                : AppColors.R5),
                  ),
                  widget.isRequired
                      ? Text(" *",
                          style: AppTextStyles.small
                              .fw400()
                              .copyWith(color: AppColors.R5))
                      : SizedBox(),
                ],
              ),
            ),
            prefixIcon: widget.prefixIcon,
            suffixIcon: widget.suffixIcon == null ? null : widget.suffixIcon,
            floatingLabelBehavior: FloatingLabelBehavior.always,
            hintText: widget.hintText,
            hintStyle: AppTextStyles.body.fw400().fcN6(),
            errorText: (widget.errorText == null || widget.errorText == '')
                ? null
                : (" " + widget.errorText!),
            errorStyle: AppTextStyles.small.fw400().fcR5(),
            contentPadding:
                EdgeInsets.only(top: 16, bottom: 12, left: 12, right: 12),
            border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(4),
              borderSide: BorderSide(
                color: AppColors.N7,
                width: 1,
              ),
            ),
            focusedBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(4),
              borderSide: BorderSide(
                color: AppColors.Pr6,
                width: 2,
              ),
            ),
            errorBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(4),
              borderSide: BorderSide(
                color: AppColors.R5,
                width: 2,
              ),
            ),
          ),
        ),
        (widget.errorText == null || widget.errorText == '')
            ? SizedBox()
            : Container(
                height: 70,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    SvgPicture.asset(AppAssetLinks.ic_error_textfield),
                  ],
                ),
              )
      ],
    );
  }
}
