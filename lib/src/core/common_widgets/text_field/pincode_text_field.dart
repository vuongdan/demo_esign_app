// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:flutter/material.dart';
import 'package:pin_code_fields/pin_code_fields.dart';

import 'package:demo_esign_app/src/core/design/app_colors.dart';
import 'package:demo_esign_app/src/core/design/app_text_styles.dart';

class PinCodeTextFieldCommon extends StatelessWidget {
  const PinCodeTextFieldCommon({
    Key? key,
    required this.controller,
    required this.onChanged,
  }) : super(key: key);
  final TextEditingController controller;
  final void Function(String) onChanged;

  @override
  Widget build(BuildContext context) {
    return PinCodeTextField(
      autoFocus: true,
      obscuringWidget: CircleAvatar(
        radius: 4,
        backgroundColor: AppColors.Pr11,
      ),
      controller: controller,
      appContext: context,
      pastedTextStyle: AppTextStyles.xSmall.fw400().fcN7(),
      textStyle: AppTextStyles.H2.fw500().fcPr11(),
      length: 6,
      blinkWhenObscuring: true,
      obscureText: true,
      animationType: AnimationType.fade,
      validator: (v) {
        return null;
      },
      pinTheme: PinTheme(
        fieldWidth: 48,
        fieldHeight: 48,
        shape: PinCodeFieldShape.box,
        borderRadius: BorderRadius.circular(4),
        activeFillColor: AppColors.Pr4,
        disabledColor: AppColors.Pr4,
        inactiveFillColor: AppColors.Pr4,
        selectedColor: AppColors.Pr4,
        inactiveColor: AppColors.Pr4,
        selectedFillColor: AppColors.Pr4,
        activeColor: AppColors.Pr4,
      ),
      cursorColor: AppColors.Pr6,
      animationDuration: Duration(milliseconds: 300),
      enableActiveFill: true,
      keyboardType: TextInputType.number,
      onCompleted: (value) {},
      onChanged: onChanged,
      beforeTextPaste: (text) {
        return true;
      },
    );
  }
}
