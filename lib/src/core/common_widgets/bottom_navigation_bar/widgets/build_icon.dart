import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:demo_esign_app/src/core/common_widgets/bottom_navigation_bar/styles/count_style.dart';
import 'package:demo_esign_app/src/core/common_widgets/bottom_navigation_bar/styles/tab_item.dart';

class BuildIcon extends StatelessWidget {
  final BottomTabItem item;
  final double iconSize;
  final Color iconColor;
  final CountStyle? countStyle;

  const BuildIcon({
    Key? key,
    required this.item,
    required this.iconColor,
    this.iconSize = 24,
    this.countStyle,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Widget icon = SvgPicture.asset(
      item.svgImageAssetLink,
      height: iconSize,
      width: iconSize,
      fit: BoxFit.scaleDown,
      color: iconColor,
    );
    if (item.count is Widget) {
      double sizeBadge = countStyle?.size ?? 18;
      return Stack(
        clipBehavior: Clip.none,
        children: [
          icon,
          PositionedDirectional(
            start: iconSize - sizeBadge / 2,
            top: -sizeBadge / 2,
            child: item.count!,
          ),
        ],
      );
    }
    return icon;
  }
}
