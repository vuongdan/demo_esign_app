import 'package:flutter/material.dart';

class BottomTabItem<T> {
  final T svgImageAssetLink;
  final String? title;
  final Widget? count;
  final String? key;

  const BottomTabItem({
    required this.svgImageAssetLink,
    this.title,
    this.count,
    this.key,
  }) : assert(svgImageAssetLink is String || svgImageAssetLink is Widget,
            'TabItem only support IconData and Widget');
}
