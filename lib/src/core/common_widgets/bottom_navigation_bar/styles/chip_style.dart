import 'package:flutter/material.dart';
import 'package:demo_esign_app/src/core/common_widgets/bottom_navigation_bar/widgets/inspired/inspired.dart';
import 'package:demo_esign_app/src/core/design/app_colors.dart';

class ChipStyle {
  final double? size;
  final Gradient? selectedGradientBackGround;
  final Gradient? unSelectedGradientBackGround;
  final Color? background;
  final Color? color;
  final bool? isHexagon;
  final bool? drawHexagon;
  final bool? convexBridge;
  final NotchSmoothness? notchSmoothness;

  const ChipStyle({
    this.size,
    this.selectedGradientBackGround = AppColors.ColorGadientBlue,
    this.unSelectedGradientBackGround = AppColors.ColorGadientFullWhite,
    this.background = Colors.blue,
    this.color,
    this.isHexagon,
    this.drawHexagon,
    this.convexBridge = true,
    this.notchSmoothness,
  });
}
