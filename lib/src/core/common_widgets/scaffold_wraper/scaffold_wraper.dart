// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:demo_esign_app/src/core/design/app_asset_links.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'package:demo_esign_app/src/core/utils/sizer/extension.dart';

class ScaffoldWraper extends StatefulWidget {
  ScaffoldWraper({
    Key? key,
    this.backGroundAssetImageLink = AppAssetLinks.img_splash_background,
    required this.child,
    this.disableWillPopScpoe = null,
  }) : super(key: key);
  final String? backGroundAssetImageLink;
  final Widget child;
  final bool? disableWillPopScpoe;

  @override
  State<ScaffoldWraper> createState() => _ScaffoldWraperState();
}

class _ScaffoldWraperState extends State<ScaffoldWraper> {
  late bool? _disableWillPopScpoe;
  late Future<bool> Function()? _onWillPop;
  @override
  void initState() {
    _disableWillPopScpoe = widget.disableWillPopScpoe;
    _onWillPop = (_disableWillPopScpoe == null) ? null : () async => false;
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
    super.initState();
  }

  @override
  void dispose() {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Container(
          height: 100.h,
          width: 100.w,
          decoration: widget.backGroundAssetImageLink != null
              ? BoxDecoration(
                  image: DecorationImage(
                    image: AssetImage(widget.backGroundAssetImageLink!),
                    fit: BoxFit.cover,
                  ),
                )
              : BoxDecoration(),
        ),
        WillPopScope(
          onWillPop: _onWillPop,
          child: widget.child,
        ),
      ],
    );
  }
}
