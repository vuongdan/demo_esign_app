// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:demo_esign_app/src/core/constants/int_constants.dart';
import 'package:flutter/material.dart';

import 'package:demo_esign_app/src/core/common_widgets/buttons/button_primary.dart';
import 'package:demo_esign_app/src/core/common_widgets/scaffold_wraper/scaffold_wraper.dart';
import 'package:demo_esign_app/src/core/design/app_asset_links.dart';
import 'package:demo_esign_app/src/core/design/app_text_styles.dart';
import 'package:demo_esign_app/src/navigations/app_navigation.dart';

class SuccessPage extends StatelessWidget {
  const SuccessPage({
    Key? key,
    required this.title,
    required this.subTitle,
    required this.routeName,
    required this.buttonContent,
  }) : super(key: key);
  final String title;
  final String subTitle;
  final String routeName;
  final String buttonContent;

  @override
  Widget build(BuildContext context) {
    return ScaffoldWraper(
      backGroundAssetImageLink: AppAssetLinks.img_splash_background,
      child: Scaffold(
        backgroundColor: Colors.transparent,
        resizeToAvoidBottomInset: false,
        body: SafeArea(
          child: Container(
            padding:
                EdgeInsets.symmetric(horizontal: DEFAULT_HORIZONTAL_PADDING),
            child: Column(
              children: [
                SizedBox(
                  height: 100,
                ),
                Padding(
                  padding: const EdgeInsets.all(75.0),
                  child: Image.asset(
                    AppAssetLinks.img_success,
                  ),
                ),
                Text(title, style: AppTextStyles.H3.fw700().fcPr11()),
                Text(
                  subTitle,
                  style: AppTextStyles.paragraph.fw400().fcPr11(),
                  textAlign: TextAlign.center,
                ),
                Spacer(),
                ButtonPrimary(
                  onTap: () {
                    AppNavigator.pushNamedAndRemoveUntil(routeName);
                  },
                  text: buttonContent,
                ),
                SizedBox(
                  height: 24,
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
