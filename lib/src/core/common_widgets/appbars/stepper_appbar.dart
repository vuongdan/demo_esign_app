import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/svg.dart';
import 'package:demo_esign_app/src/core/design/app_asset_links.dart';
import 'package:demo_esign_app/src/core/design/app_colors.dart';
import 'package:demo_esign_app/src/core/design/app_text_styles.dart';

steperAppBar({required String title, required void Function() onBack}) =>
    AppBar(
      backgroundColor: Colors.transparent,
      elevation: 0,
      systemOverlayStyle: SystemUiOverlayStyle.dark,
      bottom: PreferredSize(
        preferredSize: Size.fromHeight(1),
        child: Container(
          height: 1,
          color: AppColors.Pr1,
        ),
      ),
      leading: GestureDetector(
        onTap: onBack,
        child: Container(
          padding: const EdgeInsets.only(left: 10, right: 20),
          color: Colors.transparent,
          child: SvgPicture.asset(
            AppAssetLinks.ic_arrow_back,
            color: AppColors.N10,
            height: 16,
            width: 7,
            fit: BoxFit.scaleDown,
          ),
        ),
      ),
      title: Text(title, style: AppTextStyles.body.fw500().fcN10()),
    );
