class LocalStorageKeys {
  static const String BOX = "BOX_KEY";
  static const String ACCOUNT = "ACCOUNT";
  static const String TOKENS = "TOKENS";
  static const String SLIDES = "SLIDES2";
}
