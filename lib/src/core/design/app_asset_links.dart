// ignore_for_file: constant_identifier_names

class AppAssetLinks {
  // icons
  static const String bottom_bar_chat = 'assets/icons/bottom_bar_chat.svg';
  static const String bottom_bar_home = 'assets/icons/bottom_bar_home.svg';
  static const String bottom_bar_map = 'assets/icons/bottom_bar_map.svg';
  static const String ic_drawer_humberger =
      'assets/icons/ic_drawer_humberger.svg';
  static const String ic_notification = 'assets/icons/ic_notification.svg';
  static const String ic_search_normal = "assets/icons/ic_search_normal.svg";
  static const String ic_candle_2 = "assets/icons/ic_candle_2.svg";
  static const String ic_v = "assets/icons/ic_v.svg";
  static const String ic_tick_verify = "assets/icons/ic_tick_verify.svg";
  static const String ic_edit_2 = "assets/icons/ic_edit_2.svg";
  static const String ic_question_dialog =
      "assets/icons/ic_question_dialog.svg";
  static const String ic_privacy = "assets/icons/ic_privacy.svg";
  static const String ic_share = "assets/icons/ic_share.svg";
  static const String ic_headphone = "assets/icons/ic_headphone.svg";
  static const String ic_tutorial = "assets/icons/ic_tutorial.svg";
  static const String ic_bell = "assets/icons/ic_bell.svg";
  static const String ic_filted_docs = "assets/icons/ic_filted_docs.svg";
  static const String ic_book = "assets/icons/ic_book.svg";
  static const String ic_history = "assets/icons/ic_history.svg";
  static const String ic_change_password =
      "assets/icons/ic_change_password.svg";
  static const String ic_user_profile = "assets/icons/ic_user_profile.svg";
  static const String ic_arrow_open = "assets/icons/ic_arrow_open.svg";
  static const String ic_logout = "assets/icons/ic_logout.svg";
  static const String ic_arrow_right = "assets/icons/ic_arrow_right.svg";
  static const String ic_arrow_back = "assets/icons/ic_arrow_back.svg";
  static const String ic_arrow_down = "assets/icons/ic_arrow_down.svg";
  static const String ic_error_textfield =
      "assets/icons/ic_error_textfield.svg";
  static const String ic_show_password = "assets/icons/ic_show_password.svg";
  static const String ic_hide_password = "assets/icons/ic_hide_password.svg";
  static const String ic_tick_box = "assets/icons/ic_tick_box.svg";
  static const String ic_wv_dial = "assets/icons/ic_wv_dial.svg";
  static const String ic_wv_reload = "assets/icons/ic_wv_reload.svg";
  static const String ic_wv_share = "assets/icons/ic_wv_share.svg";
  static const String ic_wv_zoom = "assets/icons/ic_wv_zoom.svg";
  static const String ic_wv_arrow_back = "assets/icons/ic_wv_arrow_back.svg";
  static const String ic_wv_arrow_forward =
      "assets/icons/ic_wv_arrow_forward.svg";
  static const String ic_close_x = "assets/icons/ic_close_x.svg";
  static const String ic_red_error = "assets/icons/ic_red_error.svg";
  static const String ic_noti_blue = "assets/icons/ic_noti_blue.svg";

  // img
  static const String img_background = "assets/images/img_background.png";
  static const String img_calendar = "assets/images/img_calendar.png";
  static const String img_sun_and_cloud = "assets/images/img_sun_and_cloud.png";
  static const String img_magnifier = "assets/images/img_magnifier.png";
  static const String img_post = "assets/images/img_post.png";
  static const String img_search_file = "assets/images/img_search_file.png";
  static const String img_suitcase = "assets/images/img_suitcase.png";
  static const String img_education = "assets/images/img_education.png";
  static const String img_locations = "assets/images/img_locations.png";
  static const String img_technology = "assets/images/img_technology.png";
  static const String img_travel = "assets/images/img_travel.png";
  static const String img_banner = "assets/images/img_banner.png";
  static const String img_electric = "assets/images/img_electric.png";
  static const String img_phonebook = "assets/images/img_phonebook.png";
  static const String img_transfer = "assets/images/img_transfer.png";
  static const String img_water = "assets/images/img_water.png";
  static const String img_drawer = "assets/images/img_drawer.png";
  static const String img_notification = "assets/images/img_notification.png";
  static const String img_splash_background =
      "assets/images/img_splash_background.png";
  static const String img_logo = "assets/images/img_logo.png";
  static const String img_splash_a = "assets/images/img_splash_a.png";
  static const String img_splash_b = "assets/images/img_splash_b.png";
  static const String img_splash_c = "assets/images/img_splash_c.png";
  static const String img_book = "assets/images/img_book.png";
  static const String img_headphone = "assets/images/img_headphone.png";
  static const String img_message_2 = "assets/images/img_message_2.png";
  static const String img_webview_icon = "assets/images/img_webview_icon.png";
  static const String img_lock_webview = "assets/images/img_lock_webview.png";
  static const String img_register_enterprise_selected =
      "assets/images/img_register_enterprise_selected.png";
  static const String img_register_enterprise_unselected =
      "assets/images/img_register_enterprise_unselected.png";
  static const String img_register_user_selected =
      "assets/images/img_register_user_selected.png";
  static const String img_register_user_unselected =
      "assets/images/img_register_user_unselected.png";
  static const String img_success = "assets/images/img_success.png";
}
