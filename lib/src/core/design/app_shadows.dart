import 'package:flutter/material.dart';

class AppBoxShadows {
  static final List<BoxShadow> low = [
    BoxShadow(
      color: const Color(0XFF6A7381).withOpacity(0.12),
      offset: const Offset(0, 2),
      blurRadius: 8,
    )
  ];
  static const List<BoxShadow> med = [
    BoxShadow(
      color: Color(0XFF576675),
      offset: Offset(0, 3),
      blurRadius: 8,
    ),
    BoxShadow(
      color: Color(0XFF6A7381),
      offset: Offset(0, 6),
      blurRadius: 12,
    )
  ];
  static const List<BoxShadow> high = [
    BoxShadow(
      color: Color(0XFF6A7381),
      offset: Offset(0, 12),
      blurRadius: 20,
    ),
  ];
  static const List<BoxShadow> top = [
    BoxShadow(
      color: Color(0XFF6A7381),
      offset: Offset(0, -2),
      blurRadius: 8,
    ),
  ];
  static const List<BoxShadow> left = [
    BoxShadow(
      color: Color(0XFF6A7381),
      offset: Offset(-1, 2),
      blurRadius: 8,
    ),
  ];
  static const List<BoxShadow> right = [
    BoxShadow(
      color: Color(0XFF6A7381),
      offset: Offset(1, 0),
      blurRadius: 8,
    ),
  ];
}
