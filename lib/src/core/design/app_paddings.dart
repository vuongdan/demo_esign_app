import 'package:flutter/material.dart';

class AppPaddings {
  static const EdgeInsets zero = EdgeInsets.zero;
  static const EdgeInsets defaultHorizontal =
      EdgeInsets.symmetric(horizontal: 16);
}
