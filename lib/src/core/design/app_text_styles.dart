// ignore_for_file: non_constant_identifier_names, constant_identifier_names, unnecessary_this

import 'package:flutter/material.dart';
import 'package:demo_esign_app/src/core/design/app_colors.dart';

class AppTextStyles {
  /// 32/40
  static const TextStyle H1 = TextStyle(
    fontSize: 32,
    height: 40 / 32,
  );

  /// 24/32
  static const TextStyle H2 = TextStyle(
    fontSize: 24,
    height: 32 / 24,
  );

  /// 20/28
  static const TextStyle H3 = TextStyle(
    fontSize: 20,
    height: 28 / 20,
  );

  /// 16/24
  static const TextStyle paragraph = TextStyle(
    fontSize: 16,
    height: 24 / 16,
  );

  /// 16/20
  static const TextStyle body = TextStyle(
    fontSize: 16,
    height: 20 / 16,
  );

  /// 14/20
  static const TextStyle paragraphSmall = TextStyle(
    fontSize: 14,
    height: 20 / 14,
  );

  /// 14/16
  static const TextStyle small = TextStyle(
    fontSize: 14,
    height: 16 / 14,
  );

  /// 12/16
  static const TextStyle xSmall = TextStyle(
    fontSize: 12,
    height: 16 / 12,
  );

  /// 10/16
  static const TextStyle tiny = TextStyle(
    fontSize: 10,
    height: 16 / 10,
  );
}

extension ConfigStyle on TextStyle {
  // Font Weight
  TextStyle fw400() => this.copyWith(fontWeight: FontWeight.w400);
  TextStyle fw500() => this.copyWith(fontWeight: FontWeight.w500);
  TextStyle fw600() => this.copyWith(fontWeight: FontWeight.w600);
  TextStyle fw700() => this.copyWith(fontWeight: FontWeight.w700);
  TextStyle fw800() => this.copyWith(fontWeight: FontWeight.w800);

  // Font Color
  TextStyle fcR1() => this.copyWith(color: AppColors.R1);
  TextStyle fcR2() => this.copyWith(color: AppColors.R2);
  TextStyle fcR3() => this.copyWith(color: AppColors.R3);
  TextStyle fcR4() => this.copyWith(color: AppColors.R4);
  TextStyle fcR5() => this.copyWith(color: AppColors.R5);
  TextStyle fcR6() => this.copyWith(color: AppColors.R6);
  TextStyle fcR7() => this.copyWith(color: AppColors.R7);
  TextStyle fcR8() => this.copyWith(color: AppColors.R8);
  TextStyle fcR9() => this.copyWith(color: AppColors.R9);
  TextStyle fcR10() => this.copyWith(color: AppColors.R10);
  TextStyle fcD5() => this.copyWith(color: AppColors.Dark5);

  // Font Color
  TextStyle fcPr1() => this.copyWith(color: AppColors.Pr1);
  TextStyle fcPr2() => this.copyWith(color: AppColors.Pr2);
  TextStyle fcPr3() => this.copyWith(color: AppColors.Pr3);
  TextStyle fcPr4() => this.copyWith(color: AppColors.Pr4);
  TextStyle fcPr5() => this.copyWith(color: AppColors.Pr5);
  TextStyle fcPr6() => this.copyWith(color: AppColors.Pr6);
  TextStyle fcPr7() => this.copyWith(color: AppColors.Pr7);
  TextStyle fcPr8() => this.copyWith(color: AppColors.Pr8);
  TextStyle fcPr9() => this.copyWith(color: AppColors.Pr9);
  TextStyle fcPr10() => this.copyWith(color: AppColors.Pr10);
  TextStyle fcPr11() => this.copyWith(color: AppColors.Pr11);

  // Font Color
  TextStyle fcN1() => this.copyWith(color: AppColors.N1);
  TextStyle fcN2() => this.copyWith(color: AppColors.N2);
  TextStyle fcN3() => this.copyWith(color: AppColors.N3);
  TextStyle fcN4() => this.copyWith(color: AppColors.N4);
  TextStyle fcN5() => this.copyWith(color: AppColors.N5);
  TextStyle fcN6() => this.copyWith(color: AppColors.N6);
  TextStyle fcN7() => this.copyWith(color: AppColors.N7);
  TextStyle fcN8() => this.copyWith(color: AppColors.N8);
  TextStyle fcN9() => this.copyWith(color: AppColors.N9);
  TextStyle fcN10() => this.copyWith(color: AppColors.N10);

  //
  TextStyle fcV1() => this.copyWith(color: AppColors.V1);
  TextStyle fcV2() => this.copyWith(color: AppColors.V2);
  TextStyle fcV3() => this.copyWith(color: AppColors.V3);
  TextStyle fcV4() => this.copyWith(color: AppColors.V4);
  TextStyle fcV5() => this.copyWith(color: AppColors.V5);
  TextStyle fcV6() => this.copyWith(color: AppColors.V6);
  TextStyle fcV7() => this.copyWith(color: AppColors.V7);
  TextStyle fcV8() => this.copyWith(color: AppColors.V8);
  TextStyle fcV9() => this.copyWith(color: AppColors.V9);
  TextStyle fcV10() => this.copyWith(color: AppColors.V10);

  //
  TextStyle fcO1() => this.copyWith(color: AppColors.O1);
  TextStyle fcO2() => this.copyWith(color: AppColors.O2);
  TextStyle fcO3() => this.copyWith(color: AppColors.O3);
  TextStyle fcO4() => this.copyWith(color: AppColors.O4);
  TextStyle fcO5() => this.copyWith(color: AppColors.O5);
  TextStyle fcO6() => this.copyWith(color: AppColors.O6);
  TextStyle fcO7() => this.copyWith(color: AppColors.O7);
  TextStyle fcO8() => this.copyWith(color: AppColors.O8);
  TextStyle fcO9() => this.copyWith(color: AppColors.O9);
  TextStyle fcO10() => this.copyWith(color: AppColors.O10);

  //
  TextStyle fcG1() => this.copyWith(color: AppColors.G1);
  TextStyle fcG2() => this.copyWith(color: AppColors.G2);
  TextStyle fcG3() => this.copyWith(color: AppColors.G3);
  TextStyle fcG4() => this.copyWith(color: AppColors.G4);
  TextStyle fcG5() => this.copyWith(color: AppColors.G5);
  TextStyle fcG6() => this.copyWith(color: AppColors.G6);
  TextStyle fcG7() => this.copyWith(color: AppColors.G7);
  TextStyle fcG8() => this.copyWith(color: AppColors.G8);
  TextStyle fcG9() => this.copyWith(color: AppColors.G9);
  TextStyle fcG10() => this.copyWith(color: AppColors.G10);

  //
  TextStyle fcY1() => this.copyWith(color: AppColors.Y1);
  TextStyle fcY2() => this.copyWith(color: AppColors.Y2);
  TextStyle fcY3() => this.copyWith(color: AppColors.Y3);
  TextStyle fcY4() => this.copyWith(color: AppColors.Y4);
  TextStyle fcY5() => this.copyWith(color: AppColors.Y5);
  TextStyle fcY6() => this.copyWith(color: AppColors.Y6);
  TextStyle fcY7() => this.copyWith(color: AppColors.Y7);
  TextStyle fcY8() => this.copyWith(color: AppColors.Y8);
  TextStyle fcY9() => this.copyWith(color: AppColors.Y9);
  TextStyle fcY10() => this.copyWith(color: AppColors.Y10);

  //
  TextStyle fcL1() => this.copyWith(color: AppColors.L1);
  TextStyle fcL2() => this.copyWith(color: AppColors.L2);
  TextStyle fcL3() => this.copyWith(color: AppColors.L3);
  TextStyle fcL4() => this.copyWith(color: AppColors.L4);
  TextStyle fcL5() => this.copyWith(color: AppColors.L5);
  TextStyle fcL6() => this.copyWith(color: AppColors.L6);
  TextStyle fcL7() => this.copyWith(color: AppColors.L7);
  TextStyle fcL8() => this.copyWith(color: AppColors.L8);
  TextStyle fcL9() => this.copyWith(color: AppColors.L9);
  TextStyle fcL10() => this.copyWith(color: AppColors.L10);

  //
  TextStyle fcGr1() => this.copyWith(color: AppColors.Gr1);
  TextStyle fcGr2() => this.copyWith(color: AppColors.Gr2);
  TextStyle fcGr3() => this.copyWith(color: AppColors.Gr3);
  TextStyle fcGr4() => this.copyWith(color: AppColors.Gr4);
  TextStyle fcGr5() => this.copyWith(color: AppColors.Gr5);
  TextStyle fcGr6() => this.copyWith(color: AppColors.Gr6);
  TextStyle fcGr7() => this.copyWith(color: AppColors.Gr7);
  TextStyle fcGr8() => this.copyWith(color: AppColors.Gr8);
  TextStyle fcGr9() => this.copyWith(color: AppColors.Gr9);
  TextStyle fcGr10() => this.copyWith(color: AppColors.Gr10);

  //
  TextStyle fcC1() => this.copyWith(color: AppColors.C1);
  TextStyle fcC2() => this.copyWith(color: AppColors.C2);
  TextStyle fcC3() => this.copyWith(color: AppColors.C3);
  TextStyle fcC4() => this.copyWith(color: AppColors.C4);
  TextStyle fcC5() => this.copyWith(color: AppColors.C5);
  TextStyle fcC6() => this.copyWith(color: AppColors.C6);
  TextStyle fcC7() => this.copyWith(color: AppColors.C7);
  TextStyle fcC8() => this.copyWith(color: AppColors.C8);
  TextStyle fcC9() => this.copyWith(color: AppColors.C9);
  TextStyle fcC10() => this.copyWith(color: AppColors.C10);

  //
  TextStyle fcDB1() => this.copyWith(color: AppColors.DB1);
  TextStyle fcDB2() => this.copyWith(color: AppColors.DB2);
  TextStyle fcDB3() => this.copyWith(color: AppColors.DB3);
  TextStyle fcDB4() => this.copyWith(color: AppColors.DB4);
  TextStyle fcDB5() => this.copyWith(color: AppColors.DB5);
  TextStyle fcDB6() => this.copyWith(color: AppColors.DB6);
  TextStyle fcDB7() => this.copyWith(color: AppColors.DB7);
  TextStyle fcDB8() => this.copyWith(color: AppColors.DB8);
  TextStyle fcDB9() => this.copyWith(color: AppColors.DB9);
  TextStyle fcDB10() => this.copyWith(color: AppColors.DB10);

  //
  TextStyle fcGB1() => this.copyWith(color: AppColors.GB1);
  TextStyle fcGB2() => this.copyWith(color: AppColors.GB2);
  TextStyle fcGB3() => this.copyWith(color: AppColors.GB3);
  TextStyle fcGB4() => this.copyWith(color: AppColors.GB4);
  TextStyle fcGB5() => this.copyWith(color: AppColors.GB5);
  TextStyle fcGB6() => this.copyWith(color: AppColors.GB6);
  TextStyle fcGB7() => this.copyWith(color: AppColors.GB7);
  TextStyle fcGB8() => this.copyWith(color: AppColors.GB8);
  TextStyle fcGB9() => this.copyWith(color: AppColors.GB9);
  TextStyle fcGB10() => this.copyWith(color: AppColors.GB10);

  //
  TextStyle fcP1() => this.copyWith(color: AppColors.P1);
  TextStyle fcP2() => this.copyWith(color: AppColors.P2);
  TextStyle fcP3() => this.copyWith(color: AppColors.P3);
  TextStyle fcP4() => this.copyWith(color: AppColors.P4);
  TextStyle fcP5() => this.copyWith(color: AppColors.P5);
  TextStyle fcP6() => this.copyWith(color: AppColors.P6);
  TextStyle fcP7() => this.copyWith(color: AppColors.P7);
  TextStyle fcP8() => this.copyWith(color: AppColors.P8);
  TextStyle fcP9() => this.copyWith(color: AppColors.P9);
  TextStyle fcP10() => this.copyWith(color: AppColors.P10);

  //
  TextStyle fcM1() => this.copyWith(color: AppColors.M1);
  TextStyle fcM2() => this.copyWith(color: AppColors.M2);
  TextStyle fcM3() => this.copyWith(color: AppColors.M3);
  TextStyle fcM4() => this.copyWith(color: AppColors.M4);
  TextStyle fcM5() => this.copyWith(color: AppColors.M5);
  TextStyle fcM6() => this.copyWith(color: AppColors.M6);
  TextStyle fcM7() => this.copyWith(color: AppColors.M7);
  TextStyle fcM8() => this.copyWith(color: AppColors.M8);
  TextStyle fcM9() => this.copyWith(color: AppColors.M9);
  TextStyle fcM10() => this.copyWith(color: AppColors.M10);

  //
  TextStyle fcW1() => this.copyWith(color: AppColors.W1);
  TextStyle fcW2() => this.copyWith(color: AppColors.W2);
  TextStyle fcW3() => this.copyWith(color: AppColors.W3);
  TextStyle fcW4() => this.copyWith(color: AppColors.W4);
  TextStyle fcW5() => this.copyWith(color: AppColors.W5);
}
