import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:demo_esign_app/src/core/constants/local_storage_keys.dart';
import 'package:demo_esign_app/src/heplers/local_storage_path_helper.dart';
import 'package:hive/hive.dart';

class AppLocalStorage {
  static final FlutterSecureStorage secureStorage = FlutterSecureStorage();
  static Future<void> initialBox() async {
    var path = await PathHelper.appDir;
    Hive..init(path.path);
    await Hive.openBox(LocalStorageKeys.BOX);
  }
}
