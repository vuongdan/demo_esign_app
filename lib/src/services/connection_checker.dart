import 'dart:async';

import 'package:demo_esign_app/src/core/common_widgets/toasts/dialog_animation_wrapper.dart';
import 'package:demo_esign_app/src/core/common_widgets/toasts/show_error_toast.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/services.dart';

class BGeServiceConnection {
  final Connectivity _connectivity = Connectivity();
  Future<bool> checkConnection() async {
    late ConnectivityResult result;
    try {
      result = await _connectivity.checkConnectivity();
      if (result == ConnectivityResult.ethernet ||
          result == ConnectivityResult.mobile ||
          result == ConnectivityResult.wifi) {
        return true;
      }
    } on PlatformException catch (e) {
      print(e);
      return false;
    }
    showToast(ToastType.error, title: "Mất kết nối, xin vui lòng thử lại");
    return false;
  }
}
