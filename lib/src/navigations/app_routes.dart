// ignore_for_file: constant_identifier_names

class Routes {
  static const String ROOT = '/';
  static const String SPLASH_LOADING = '/SPLASH_LOADING';
  static const String SPLASH_SLIDE = '/SPLASH_SLIDE';
  static const String SPLASH_LOADED = '/SPLASH_LOADED';
  static const String HOME = '/HOME';
  static const String LOGIN = '/LOGIN';
  static const String LOGIN_WEBVIEW = '/LOGIN_WEBVIEW';
  static const String REGISTER_SELECT_METHOD = '/REGISTER_SELECT_METHOD';
  static const String REGISTER_USER = '/REGISTER_USER';
  static const String REGISTER_OTP = '/REGISTER_OTP';
  static const String SUCCESS = '/SUCCESS';
  static const String REGISTER_ENTERPRISE = '/REGISTER_ENTERPRISE';
  static const String FORGOT_PASSWORD = '/FORGOT_PASSWORD';
  static const String FORGOT_PASSWORD_OTP = '/FORGOT_PASSWORD_OTP';
}
