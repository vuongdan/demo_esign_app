// ignore_for_file: unused_local_variable

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:demo_esign_app/src/core/common_widgets/scaffolds/success_scaffold.dart';
import 'package:demo_esign_app/src/navigations/app_navigator_observer.dart';
import 'package:demo_esign_app/src/navigations/app_routes.dart';
import 'package:demo_esign_app/src/navigations/app_transition_route.dart';

final GlobalKey<NavigatorState> navigatorKey = GlobalKey();

class AppNavigator extends RouteObserver<PageRoute<dynamic>> {
  Route<dynamic>? getRoute(RouteSettings settings) {
    Map<String, dynamic>? arguments = _getArguments(settings);

    switch (settings.name) {
      default:
        return null;
    }
  }

  _buildRoute(
    RouteSettings routeSettings,
    Widget builder,
  ) {
    return AppMaterialPageRoute(
      builder: (context) => builder,
      settings: routeSettings,
    );
  }

  _getArguments(RouteSettings settings) {
    return settings.arguments;
  }

  static Future push<T>(
    String route, {
    Object? arguments,
  }) {
    return state.pushNamed(route, arguments: arguments);
  }

  static Future pushNamedAndRemoveUntil<T>(
    String route, {
    Object? arguments,
  }) {
    return state.pushNamedAndRemoveUntil(
      route,
      (route) => false,
      arguments: arguments,
    );
  }

  static Future replaceWith<T>(
    String route, {
    Map<String, dynamic>? arguments,
  }) {
    return state.pushReplacementNamed(route, arguments: arguments);
  }

  static void popUntil<T>(String route) {
    state.popUntil(ModalRoute.withName(route));
  }

  static void pop() {
    if (canPop) {
      state.pop();
    }
  }

  static bool get canPop => state.canPop();

  static String? currentRoute() => AppNavigatorObserver.currentRouteName;

  static BuildContext? get context => navigatorKey.currentContext;

  static NavigatorState get state => navigatorKey.currentState!;
}
