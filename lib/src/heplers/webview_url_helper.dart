String webviewUrl(String url) {
  final int index = url.indexOf("?");
  if (index != -1) {
    return url.substring(0, index);
  } else {
    return url;
  }
}

String? webviewUrlCatchAuthCode(String url) {
  final int startIndex = url.indexOf("code=");
  final int endIndex = url.indexOf("&");
  if (startIndex != -1 && endIndex != -1) {
    return url.substring(startIndex + 5, endIndex);
  } else {
    return null;
  }
}
