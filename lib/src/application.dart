import 'dart:html';

import 'package:demo_esign_app/src/core/utils/sizer/widget.dart';
import 'package:demo_esign_app/src/features/splash/presentation/pages/splash_page.dart';
import 'package:demo_esign_app/src/navigations/app_navigation.dart';
import 'package:demo_esign_app/src/navigations/app_navigator_observer.dart';
import 'package:demo_esign_app/src/navigations/app_routes.dart';
import 'package:flutter/material.dart';

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Sizer(
      builder: ((context, orientation, deviceType) {
        return MaterialApp(
          debugShowCheckedModeBanner: false,
          theme: ThemeData(
            applyElevationOverlayColor: true,
          ),
          builder: (context, child) {
            return MediaQuery(
              data: MediaQuery.of(context).copyWith(textScaleFactor: 1.0),
              child: child!,
            );
          },
          navigatorKey: navigatorKey,
          navigatorObservers: [
            AppNavigatorObserver(),
          ],
          initialRoute: Routes.ROOT,
          onGenerateRoute: (settings) {
            return AppNavigator().getRoute(settings);
          },
          home: SplashPage(),
        );
      }),
    );
  }
}

class ApplicationData {
  static const String MODE = "DEV";
}
