import 'package:demo_esign_app/src/core/common_widgets/scaffold_wraper/scaffold_wraper.dart';
import 'package:demo_esign_app/src/core/utils/sizer/extension.dart';
import 'package:flutter/material.dart';

class SplashPage extends StatefulWidget {
  const SplashPage({Key? key}) : super(key: key);

  @override
  State<SplashPage> createState() => _SplashPageState();
}

class _SplashPageState extends State<SplashPage> {
  @override
  Widget build(BuildContext context) {
    return ScaffoldWraper(
      child: Scaffold(
        backgroundColor: Colors.black,
        body: Container(
          height: 100.h,
          width: 100.w,
          color: Colors.black,
        ),
      ),
    );
  }
}
