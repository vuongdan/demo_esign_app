import 'package:demo_esign_app/src/core/utils/logger/until_loger.dart';
import 'package:demo_esign_app/src/services/app_local_storage.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:demo_esign_app/src/application.dart';
import 'package:demo_esign_app/src/core/bloc/app_blocs_provider.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await AppLocalStorage.initialBox();
  Bloc.observer = AppBlocObserver();
  runApp(MultiBlocProvider(providers: AppBlocs.blocProviders, child: MyApp()));
}

class AppBlocObserver extends BlocObserver {
  @override
  void onEvent(Bloc bloc, Object? event) {
    UtilLogger.log('ESIGN EVENT', event);
    super.onEvent(bloc, event);
  }

  @override
  void onError(BlocBase bloc, Object error, StackTrace stackTrace) {
    UtilLogger.log('ESIGN ERROR', error);
    super.onError(bloc, error, stackTrace);
  }

  @override
  void onTransition(Bloc bloc, Transition transition) {
    UtilLogger.log('ESIGN TRANSITION', transition.event);
    super.onTransition(bloc, transition);
  }
}
